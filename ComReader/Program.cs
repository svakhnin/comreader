﻿#region Namespace Inclusions
using System;
using System.IO;
using System.IO.Ports;
#endregion

namespace ComReader
{
    class Program
    {
        private SerialPort port = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One);        
        private RRDataDecoder decoder = new RRDataDecoder();

        public Program() {
            // Attach a method to be called when there
            // is data waiting in the port's buffer
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            // Begin communications
            port.Open();
        }
        
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bufferSize = 8;
            byte[] buff = new byte[bufferSize];
            int k = port.Read(buff, 0, bufferSize);
            for (int i = 0; i < k; i++) {
                decoder.push(buff[i]);
            }            
        }

        static void readCom() {
            Console.WriteLine("start-com");
            new Program();  
        }

        static void readFile() {
            Console.WriteLine("start-file");

            //String fileName = "D:\\src\\ComReader\\ComReader\\test-data.log";
            //String fileName = "D:\\src\\ComReader\\ComReader\\output1_2017-09-14_16-07-05.log"; //113 - 71-48
            String fileName = @"D:\src\ComReader\ComReader\output2_2017-09-14_16-10-44.log"; //191-153-60

            byte[] data = File.ReadAllBytes(fileName);
            
            RRDataDecoder decoder = new RRDataDecoder();
            for (int i = 0; i < data.Length; i++) {
                decoder.push(data[i]);
            }

            Console.WriteLine("File size: "+data.Length);
        }

        [STAThread]
        static void Main(string[] args)
        {
            //readCom();
            readFile();
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }
    }
}