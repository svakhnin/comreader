﻿using System;


namespace ComReader
{
    public class RRDataDecoder
    {

        private byte count = 0;
        private int systRR = 0;        
        private byte diastRR = 0;
        private byte heartRate = 0;
        private static byte[] startSeqPattern = { 255, 254, 8 };



        public void push(byte b)
        {
            switch (count)
            {
                case 0: // check start sequence
                case 1:
                case 2:
                    {
                        if (startSeqPattern[count] == b)
                        {
                            count++;
                        }
                        else
                        {                            
                            count = 0;
                        }
                        break;
                    }
                case 3: // unknown bytes
                case 4:
                    {
                        count++;
                        break;
                    }
                case 5: // check 6th 0 - byte
                    {
                        if (b == 0)
                        {
                            count++;
                        }
                        else
                        {
                            count = 0;
                        }
                        break;
                    }
                case 6: // Syst. RR low byte 
                    {
                        systRR = b;
                        count++;
                        break;
                    }
                case 7: // Syst. RR high byte
                    {
                        systRR = 256 * b + systRR;
                        count++;
                        break;
                    }
                case 8: //  Diast. RR
                    {
                        diastRR = b;
                        count++;
                        break;
                    }
                case 9: // heart rate
                    {
                        heartRate = b;
                        count = 0;
                        sucsessMeasurment(systRR, diastRR, heartRate);
                        break;
                    }
                default:
                    { // should never enter this block
                        Console.WriteLine("Wrong byte count");
                        count = 0;
                        break;
                    }
            }
        }

        public void sucsessMeasurment(int systRR, byte diastRR, byte heartRate)
        {            
            Console.WriteLine("S: " + systRR + ", D: " + diastRR + ", HR: " + heartRate);
        }
    }
}



